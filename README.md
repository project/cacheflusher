## Cache flusher

The cache flusher module adds a link to the navigation toolbar
that allows users to clear drupal's cache. The module provides
a permission, that can be given to user roles, to allow only
certain users to have access to the cacheflusher.

Once cache flusher has cleared drupal's cache, the module takes
you back to the page that the user was on when the cacheflusher
button was pressed.
